#!/bin/sh
         if [ "$CI_COMMIT_BRANCH" == "main" ] ; then
         tag=$( ( git tag | egrep v[0-9]\.[0-9]\.[0-9] || echo v1.0.-1 ) | sort -V -r | head -1 | awk -F. -v OFS=. '{$NF++;print}' )
         fi
         if  [ "$CI_COMMIT_BRANCH" == "dev" ] ; then
         tag="dev-$(date +'%s')"
         fi
         if [ "$CI_COMMIT_BRANCH" == "release" ] ; then
         tag=$( ( git tag | egrep rc-[0-9] || echo rc-0 ) | sort -V -r | head -1 | awk -F 'rc-' '{ print "rc-" $2 +1}' )
         fi
         git add .
         docker build --pull -t "$GCR_IMAGE:${tag}" .
         docker push "$GCR_IMAGE:${tag}"
         git tag $tag
         export tag
         git push origin $tag
         echo $tag