#!/bin/sh

FAILURE="failed"
SUCCESS="success"

slack_msg_header=":x: Build failed"

if [ "${CI_JOB_STATUS}" = "${SUCCESS}" ]
then
    slack_msg_header=":white_check_mark: Build success"
fi
if [ "${CI_JOB_STATUS}" = "${FAILURE}" ]
then
    slack_msg_header=":white_check_mark: Build failed"
fi
curl -X POST "${SLACK_WEBHOOK}" -d @- <<SLACK
{
  "blocks": [
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "${slack_msg_header}"
      }
    },
    {
      "type": "divider"
    },
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": " Image ${tag}"
      }   
    },
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "*Stage:*\nBuild"
      }
    },
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "*Pushed By:*\n${GITLAB_USER_NAME}"
      }
    },
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "*Job URL:*\nhttps://gitlab.com/y.osherovych/test/-/jobs//${CI_JOB_ID}"
      }
    },
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",
        "text": "*Commit URL:*\nhttps://gitlab.com/y.osherovych/test/-/commit//$(git rev-parse HEAD)"
      }
    },
    {
      "type": "section",
      "text": {
        "type": "mrkdwn",  
        "text": "*Commit Branch:*\nhttps://gitlab.com/y.osherovych/test/-/tree//${CI_COMMIT_BRANCH}"
      }
    }
  ]
}
SLACK
